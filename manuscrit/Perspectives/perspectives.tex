\stoptocwriting
Le travail de recherche que nous présentons dans ce manuscrit soulève de
nombreuses pistes de recherches. Ce dernier chapitre fait un bilan des voies qui
nous paraissent intéressantes à explorer.

\section*{Interprétation combinatoire de l'état stationnaire de l'ASEP}

Un des outils principaux pour étudier les probabilités de l'état stationnaire de
l'ASEP est l'\ansatz{} matriciel de Derrida, Evans, Hakim et Pasquier
\cite{DEHP}. Dans nos travaux, nous utilisons la généralisation
introduite par Corteel et Williams \cite{CWte} :
\begin{equation}\label{eq:per_sys_ansatz_new}
    \hspace{-.6cm}
    \left \{
        \begin{array}{llll}
            ug^\lambda(X\no\bl Y)
            &= qg^\lambda(X\bl\no Y)
            &+ \lambda_{|X|+|Y|+2}\left(g^\lambda(X\no Y)+g^\lambda(X\bl Y)\right)
            & (I)
            \\
            \alpha g^\lambda(\bl X)
            &= \gamma g^\lambda(\no X)

            &+ \lambda_{|X|+1} g^\lambda(X)
            & (II)
            \\
            \beta g^\lambda(X\no)
            &= \delta g^\lambda(X\bl)
            &+ \lambda_{|X|+1} g^\lambda(X)
            & (III)
            \\
            g^\lambda(\varepsilon)
            &= \lambda_0.
            &
            &
        \end{array}%
    \right.\hspace{-.9cm}%
\end{equation}%
À partir de ces récurrences locales, nous avons montré que le poids
$g^\lambda(m)$ d'un mot $m\in\{\bl,\no\}^n$ peut s'écrire comme une combinaison
linéaire de poids de mots de taille $n-1$ :
\[
    g^\lambda(m)=\sum_{m'\in\{\bl,\no\}^{n-1}}a_{m,m'}g^\lambda(m').
\]
Nous obtenons ainsi des récurrences dites globales, qui admettent exactement une
solution. Le théorème~\ref{th:asep_rec} montre que cette solution vérifie le
système~\eqref{eq:per_sys_ansatz_new}. Ainsi, pour montrer qu'une classe
combinatoire $\mathcal C$ donne une interprétation des probabilités de l'état
stationnaire de l'ASEP, il suffit de définir une surjection $\rho:\mathcal
C_n\longrightarrow \{\bl,\no\}^n$ et un poids $w$, de sorte que
\begin{equation}\label{eq:per_classe_rec_globales}
    \mathfrak C_m=\sum_{m'\in\{\bl,\no\}^{n-1}}a_{m,m'}\mathfrak C_{m'},
\end{equation}
où $\mathfrak C_m=\sum_{C\in\rho^{-1}(m)}w(C)$. À travers un algorithme
d'insertion, nous avons montré combinatoirement que les tableaux escaliers
vérifient l'équation~\eqref{eq:per_classe_rec_globales}. À notre connaissance,
la seule autre preuve montrant que ces tableaux donnent une interprétation
combinatoire aux probabilités stationnaires de l'ASEP est due à Corteel et
Williams \cite{CWte}, qui utilisent directement le
système~\eqref{eq:per_sys_ansatz_new}. Cependant, la démonstration est longue et
calculatoire. La preuve actuelle du théorème~\ref{th:asep_rec} repose sur
l'existence d'une solution à l'\ansatz{} matriciel. À notre connaissance, la
seule solution est celle proposée par \cite{CWte} qui correspond justement à la
preuve que les tableaux escaliers satisfont l'\ansatz{} matriciel. Pour achever
notre travail, il faudrait démontrer l'existence d'une solution sans utiliser
les tableaux escaliers. Par exemple, en interprétant l'\ansatz{} matriciel comme
un système linéaire, nous pourrions appliquer les outils de l'algèbre linéaire.
Une alternative serait de montrer directement que les récurrences globales
impliquent les récurrences du système~\eqref{eq:per_sys_ansatz_new}. Nous sommes
dans la même situation pour l'ASEP à deux types de particules
(chapitre~\ref{ch:ASEPdeux}).

Concernant les tableaux escaliers de type $B$, nous avons défini deux \ansatz{}
matriciel (équations~\eqref{eq:asep_sys_ansatz_Bab} et
\eqref{eq:asep_sys_ansatz_Bad} du chapitre~\ref{ch:ASEP}) : un pour le type $\Bab$
et un autre pour le type $\Bad$. Pour chacun des deux, nous définissons une
récurrence globale dont nous donnons une interprétation par un algorithme
d'insertion sur les tableaux escaliers de type $B$. Cette fois-ci, il nous
manque la preuve de l'existence d'une solution aux \ansatz{} matriciels. Les
équivalents du théorème~\ref{th:asep_rec} sont les
conjectures~\ref{conj:asep_rec_Bab} et \ref{conj:asep_rec_Bad}.
\\

Nous avons montré dans le chapitre~\ref{ch:ASEPdeux} que notre démarche du
chapitre~\ref{ch:ASEP} se généralise au cas de l'ASEP à deux particules.
Naturellement, la question se pose de la possibilité d'appliquer notre méthode à
un ASEP plus complexe. Pour un ASEP donné, la première
étape serait de définir un \ansatz{} matriciel s'il n'y en a pas. Ensuite, il
faudrait en déduire une récurrence globale. Enfin, cette récurrence globale nous
imposerait un algorithme d'insertion qu'il faudrait appliquer à une forme de
tableau adéquat pour en déduire les objets combinatoires correspondants.
L'extension la plus simple devrait être de différencier la probabilité $q$ suivant les
types des deux particules impliquées. Nous obtiendrions les mêmes tableaux mais
avec un poids différent pour les rhombes verticaux et horizontaux.
Mandelshtam \cite{MmultiASEP} étudie un ASEP avec plusieurs types de
particules grises avec la spécialisation $\gamma=\delta=0$. Elle y définit un
\ansatz{} matriciel ainsi que des tableaux le satisfaisant. Pour chaque type de
particule grise un pas diagonal différent est associé. Par notre méthode,
nous pouvons espérer définir une généralisation à la fois de ces tableaux et des
tableaux escaliers rhombiques. Toutefois, pour prouver que ces nouveaux tableaux
donnent une interprétation combinatoire de cet ASEP, il nous manquerait toujours
la dernière brique : montrer que les équations globales impliquent les
équations locales. Cantini, Garbali, de Gier et Wheeler \cite{CGdGW} étudient un
ASEP avec plusieurs types de particules noires et blanches. Dans leurs travaux,
ils n'utilisent pas d'\ansatz{} matriciel et il ne définissent pas non plus de
tableaux. Même si on devine ce que serait l'\ansatz{} matriciel correspondant,
plusieurs récurrences globales peuvent être définies et il n'est pas clair que
nous puissions l'interpréter en un algorithme d'insertion sur des tableaux.

Il serait également intéressant de voir si notre approche peut être adaptée à
l'ASEP sur les autres types de réseaux. Une première étape pourrait être
d'étudier l'ASEP à deux particules sur $\ZZ/n\ZZ$. En
effet, ce modèle, avec les restrictions $\gamma=\delta=q=0$, a été étudié par
Mandelshtam \cite{Mtoric} qui en s'appuyant sur l'\ansatz{} matriciel
correspondant, introduit par Derrida, Evans, Hakim et Pasquier \cite{DEHP},
définit des tableaux interprétant l'état stationnaire.
\\

Les types $B$ des tableaux alternatifs, des tableaux boisés et des tableaux
escaliers sont obtenus comme points fixes d'une simple involution : la symétrie
axiale d'axe la diagonale principale.
Pour le cas $\gamma=\delta=0$, cette involution correspond à
un relèvement de la symétrie CP du ASEP (remarque~\ref{rem:ASEP_CP_c_d_0}).
Quant à la spécialisation $q=1$, les involutions à l'origine des tableaux
escaliers de type $\Bab$ et $\Bad$ sont respectivement les symétries CP et C.
L'ASEP à deux particules possède également les symétries P, C et CP. La nature
des pavages des diagrammes rhombiques rend plus difficile la définition des
involutions. Notamment pour les tableaux escaliers rhombiques pour lesquels la
forme du pavage est fixé. On pourrait néanmoins étudier les tableaux alternatifs
rhombiques dont le pavage ainsi que le remplissage par $\alpha$ et $\beta$ est
stable par transposition. D'après \cite[Figure~11]{MV} le poids en $q$ devrait
être stable.
\\

L'interprétation combinatoire de l'ASEP par les tableaux encode l'état associé à
un tableau sur le bord sud-est. Dans le chapitre~\ref{ch:coins}, nous avons
énuméré les coins permettant ainsi de calculer le nombre moyen de transitions
possibles depuis un état pour $\gamma=\delta=0$ et $\alpha=\beta=q=1$. Yan et
Zhou ont traité le cas $\alpha$ et $\beta$ généraux en reformulant le problème
en termes de partitions liées. Il reste à interpréter le poids $q$ directement
sur les partitions liées pour traiter le cas $q$
général. Dans la section~\ref{sec:asep_coins_debut_preuve}, nous proposons une
méthode alternative à celle de Yan et Zhou. Elle est inspirée de la
construction qui nous permet de donner le polynôme générateur des coins
occupés pour $\alpha$, $\beta$ et $q$ généraux
(proposition~\ref{prop:asep_co_abq}). Pour traiter le cas des coins, nous
partitionnons les coins vides en quatre types de coins vides et nous calculons
les polynômes générateurs de trois d'entre eux. Au vu de la forme factorisée du
polynôme générateur des coins restants, nous pouvons espérer trouver une preuve
combinatoire directe. Prendre en compte le poids en $q$ semble tout de même
compliqué.

Il y a deux façons de généraliser le calcul du nombre moyen de transitions
possibles depuis un état. La première est de considérer $\gamma$ et
$\delta$ généraux. La notion de coin est alors remplacée par la succession de
$\alpha/\delta$ par $\beta/\gamma$ dans les cellules sud-est des tableaux
escaliers. La deuxième est de traiter le cas de l'ASEP à
deux particules en commençant par les tableaux alternatifs rhombiques.

Outre les formules factorisées qu'ils engendrent, la motivation principale de
l'étude des coins est leur interprétation sur l'ASEP comme les lieux où un saut
de particule est possible. Il serait intéressant de développer l'interprétation
physique de nos résultats.






\section*{Arbres non-ambigus}

Dans le chapitre~\ref{ch:ana}, nous avons trouvé de jolies formules closes pour la
série génératrice et pour le polynôme générateur des arbres non-ambigus, en
prenant notamment en compte le nombre de sommets dans la première ligne ainsi
que le nombre de sommets dans la première colonne. Ces deux statistiques
correspondent respectivement
aux paramètres $\beta$ et $\alpha$. Nous les avons
obtenus de deux façons différentes. D'une part à l'aide d'une équation
différentielle vérifiée par la série génératrice et d'autre part grâce à une
interprétation combinatoire utilisant la décomposition d'un arbre binaire en
équerres. Dans le contexte de l'ASEP avec $\gamma=\delta=0$, des questions
naturelles se posent. Peut-on raffiner l'une ou l'autre des méthodes pour
introduire également le paramètre $q$ ? Que peut-on déduire de la décomposition
en équerres sur les tableaux boisés ?

Les arbres non-ambigus correspondent aux tableaux boisés de forme
rectangulaire. Les tableaux qui généralisent les tableaux boisés et qui
interprètent l'ASEP à deux particules sont les tableaux alternatifs rhombiques.
On pourrait définir les \emph{arbres non-ambigus rhombiques} comme les tableaux
alternatifs rhombiques dont le bord sud-est est composé de pas horizontaux puis
de pas diagonaux et enfin de pas verticaux. Même si elle dépend du pavage, les
tableaux alternatifs rhombiques ont également une structure arborescente
sous-jacente que l'on pourrait étudier.
\\

Un travail qui n'apparaît pas dans ce manuscrit est la recherche d'une structure
d'algèbre de Hopf sur les arbres non-ambigus. Les arbres non-ambigus
correspondent à des classes d'équivalence de tableaux boisés. Or, les tableaux
boisés sont en bijection avec les permutations. De plus, il existe une
surjection naturelle des arbres non-ambigus vers les arbres binaires. Notre idée
initiale était donc de construire une algèbre de Hopf qui s'intercale entre
$\FQSym$, l'algèbre de Hopf des permutations de Malvenuto et Reutenauer
\cite{MR}, et $\PBT$, l'algèbre de Hopf des arbres binaires de Loday et
Ronco \cite{LR}. Toutefois, en utilisant les relations des algèbres de Hopf, on
peut montrer que si une telle inclusion d'algèbres de Hopf existe, alors le
nombre d'arbres non-ambigus de taille $5$ devrait être plus grand que $63$. Ce
qui n'est pas possible étant donné qu'il y en a $62$. Suite à cela, nous avons
privilégié l'algèbre de Hopf $\PBT$. Une propriété de $\PBT$ est
que le produit de deux intervalles du treillis de Tamari \cite{HT} est également
un intervalle. Il paraissait donc naturel de construire 
un treillis sur les arbres non-ambigus en parallèle de la structure d'algèbre de
Hopf. Malheureusement, ce travail n'a pas abouti.
\\

Dans le chapitre~\ref{ch:ana_gen}, nous avons donné une généralisation des arbres
non-ambigus en dimension supérieure. Pour l'instant, les seuls résultats que nous
avons à propos de ces nouveaux objets sont des équations différentielles
vérifiées par leurs séries génératrices. Contrairement à la dimension $2$, nous
n'avons pas réussi à les résoudre. Pour cela, nous pensons qu'il faudrait mieux
comprendre les liens entre les différentes dimensions. Pour le moment nous
avons uniquement l'identité
\(
    \left.\ANAFGddk\right|_{x_d=0}=\ANAFGddk[d-1][k].
\)

Dans l'article \cite{ABBS}, les auteurs étudient les arbres non-ambigus
complets. C'est-à-dire, les arbres non-ambigus dont l'arbre binaire sous-jacent
est complet. Entre autre, ils montrent que leur série génératrice est égale au
logarithme de la fonction de Bessel $J_0$. On pourrait peut-être obtenir un
résultat similaire en dimension supérieure.

Une dernière piste de recherche est la généralisation des tableaux boisés en
dimension supérieure. Ils mèneraient peut-être à un ASEP à plusieurs dimensions.


\section*{Polyomino parallélogrammes périodiques}

Nos travaux sur les polyominos parallélogrammes périodiques se basent sur une
structure arborescente que l'on fait apparaître en ajoutant un point en haut des
colonnes et à droite des lignes. Nous trouvons ainsi la série génératrice de ces
objets en prenant en compte la hauteur et la largeur ainsi que deux nouvelles
statistiques : la largeur intrinsèque et la hauteur de recollement intrinsèque.
Actuellement, la structure arborescente ne permet toutefois pas d'étudier
l'aire. La méthode de Bousquet-Mélou et Viennot \cite{BMV}
utilisée par Biagioli, Jouhet et Nadeau \cite{BJN}, permet de calculer la série
génératrice en prenant également l'aire en compte. Toutefois, la symétrie
$x\leftrightarrow y$ de l'objet n'apparait pas dans leurs résultats. Notre
approche respectant cette symétrie, il serait intéressant de trouver un
encodage de l'aire, simple et symétrique, dans nos structures arborescentes
pour obtenir une série génératrice symétrique en $x$ et $y$. Une solution
serait de construire une partition des cellules, compatible avec la
symétrie, de sorte qu'il y ait exactement une cellule pointée par sous-ensemble.
Nous obtiendrions un étiquetage de la structure sous-jacente
dont la somme serait égale à l'aire. Par exemple, soit $\mathcal E$ une ensemble
maximal de cellules dont les coins sud-ouest appartiennent à la même droite de
pente $1$. En lisant $\mathcal E$ du nord-est au sud-ouest, on regroupe une cellule
pointée avec les cellules vident qui la suivent. Cette construction est
bien stable par la symétrie d'axe $\Delta:y=x$. 

Un polyomino parallélogramme périodique $\ppp$ est entièrement déterminé par sa
structure arborescente $\zeta(\ppp)$ et sa hauteur de recollement intrinsèque,
qui sont indépendants. En particulier, la hauteur de recollement intrinsèque
partitionne l'ensemble infini des polyominos parallélogrammes périodiques de
demi-périmètre fixé en sous-ensembles de même taille finie. Toutefois, c'est une
statistique qui est difficile à lire directement sur l'objet. Indépendamment de
nos travaux, Biagioli, Jouhet et Nadeau \cite{BJN} ont introduit les polyominos
parallélogrammes périodiques en montrant qu'ils sont en bijection avec les
permutations affines qui évitent $321$. Il serait intéressant de pouvoir
interpréter la hauteur de recollement intrinsèque sur ces permutations ou sur
d'autres objets combinatoires.

Nous avons interprété les polyominos parallélogrammes périodiques de hauteur
intrinsèque $1$ comme l'aire triangulaire sous les chemins de Dyck. Or, la
formule d'énumération des polyominos parallélogrammes périodiques
intrinsèquement minces nous dit que ces derniers peuvent aussi être interprétés comme
l'aire triangulaire, avec une multiplicité induite par les pics, sous les
chemins de Dyck. Il reste à trouver une bijection interprétant les deux à la
fois.

La construction de \cite{ABBS} mettant en bijection les polyominos
parallélogrammes avec les arbres binaires consiste à construire un
arbre non-ambigu à partir d'un polyomino parallélogramme en ajoutant des points
à droite des lignes et en haut des colonnes. À un facteur de renormalisation
près, l'aire correspond ainsi au paramètre $q$ de l'ASEP. Peut-on étendre ce
lien au cas des polyominos parallélogrammes périodiques ?


L'objectif initial qui était d'étudier les polyominos convexes à partir des
polyominos parallélogrammes périodiques n'a pas été traité. Maintenant que nous
avons une meilleure compréhension de ces nouveaux objets, il pourrait être
intéressant de revenir à cette idée.


\resumetocwriting
