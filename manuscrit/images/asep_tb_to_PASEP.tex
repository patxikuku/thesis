\documentclass{standalone}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}

\usepackage{graphicx}
\usepackage{tikz,pgf}
\usepackage{amsmath}
\usepackage{amssymb,amsthm}
\usepackage{mathtools}
\usepackage{mathrsfs}
\usepackage{stmaryrd}
\usepackage{color}%pour pouvoir changer la couleur du text

\usetikzlibrary{arrows} 

%Pour dessiner les tableaux boisÃ©s.
\usetikzlibrary{shapes.geometric}

  \newcommand{\LineWidth}{1}
  \newcommand{\SizePoint}{.10*\LineWidth}
  
\newcommand{\Square}[2]{
    \draw[color=black, line width=\LineWidth] (#1-.5, #2-.5) --++ (0,1) --++ (1,0)--++ (0,-1) -- cycle;
    }
\newcommand{\Point}[2]{
    \filldraw[color=black] (#1, #2) circle (\SizePoint);
    }

%Pour colorier des carrés
\newcommand{\ColoredSquare}[3][white]{
    \filldraw[color=#1] (#2-.5, #3-.5) --++ (1,0) --++ (0,1) --++ (-1,0) -- cycle;
    }
    
%Pour marquer les carrés du ruban
\newcommand{\RibbonSquare}[2]{
    \draw (\x,\y) node {\color{red}\Large\textbf{+}}
    }
    
%Pour le point spécial
\tikzset{block/.style    = {draw, rectangle, minimum height = \SizePoint,
   minimum width = \SizePoint, inner sep=5pt}}
\newcommand{\SpecialPoint}[2]{
    \draw (#1, #2) node [block] {};
    }

\renewcommand{\no}{{\bullet}}
\newcommand{\bl}{{\circ}}
\begin{document}
\begin{tikzpicture}

\begin{scope}[scale=.9]
    \foreach \x/\y in {0/0,0/1,0/4,0/5,1/3,1/5,2/2,2/5,3/3,4/4,5/5}{
        \Point{\x}{\y}
    }
    \foreach \xmax [count=\y from 0] in {0,0,3,4,4,5}{
        \foreach \x in {0,...,\xmax}{
            \Square{\x}{\y}
        }
    }
    %indice  horizontaux
    \foreach \x/\y in {1/2,2/2,3/2,4/3,5/5}{
        \node[anchor=south] at (\x+.1,\y-1) {\LARGE$\circ$};
    }

    %indice  verticaux
    \foreach \x/\y in {0/0,0/1,3/2,4/3,4/4}{
        \node at (\x+.75,\y-.1) {\LARGE$\bullet$};
    }
\end{scope}

\begin{scope}[xshift=175,yshift=70]
    \draw[->,line width=2*\LineWidth] (0,0)-- node[above] {\huge$\varphi$} ++ (1.5,0) ;
\end{scope}


\begin{scope}[xshift=300,yshift=70]
    \node at (0,0) {\LARGE$\bl\no\no\bl\no\bl\bl\bl\no\no$};
\end{scope}

\end{tikzpicture}
\end{document}
